#
# build.tcl: Tcl script for re-creating project 'crono-tdc'
#
# Usage:
# 1) Source the tcl script `source build_project.tcl`. This will add the required 
# commands to the Tcl console.
# 2) Execute the command `create_vivado_project` with the desired arguments.
# In example: `create_vivado_project -p xc7a35ticsg324-1L -d design_name`
#
# Notes about parts:
# Is the PN of the FPGA chip, for example:
# * For arty-a735: xc7a35ticsg324-1L
#
#*****************************************************************************************

proc glob_for_sources {dir} {
  #-------------------------------------------------------
  # Recursively find sources inside ${dir} directory.
  #-------------------------------------------------------
    set res {}
    foreach i [lsort [glob -nocomplain -dir $dir *]] {
        # if it is a directory then call again this function (recursive)
        puts "analyzing ${i}..."
        if {[file type ${i}] eq {directory}} {
          puts "it is a directory! looking inside..."
            eval lappend res [glob_for_sources ${i}]
        } else {
          # append if it is a verilog source
            if { ([file extension ${i}] eq {.v}) || ([file extension ${i}] eq {.sv}) } {
              puts "${i} is a verilog or system verilog file! including it..."
              lappend res ${i}
            } else {
              puts "${i} is not a verilog or system verilog file! skip..."
            }
        }
    }
    puts "encountered source files: ${res}"
    set res
} ;# RS


proc lshift listVar {
  #-------------------------------------------------------
  # Just used to shift arguments of `create_vivado_project` proc.
  #-------------------------------------------------------
  upvar 1 $listVar L
  set r [lindex $L 0]
  set L [lreplace $L [set L 0] 0]
  return $r
}


proc create_vivado_project {args} {
  #-------------------------------------------------------
  # Process command line arguments
  #-------------------------------------------------------
  # constants --------------------------------------------
  set VIVADO_VERSION_REQUIRED "2018.3"
  # Set the reference directory for source file relative paths (by default the value is script directory path)
  set origin_dir "."

  # variables that could be set with arguments -----------
  set help 0
  set fpga_part "none"
  set design_name "none"
  set xdc_filename "none"
  set sources_dir "none"
  set top_level_filename "none"

  # if {[llength $args] == 0} { incr help }; # Uncomment if necessary
  while {[llength $args]} {
    set flag [lshift args]
    switch -exact -- $flag {
      -p -
      -part {
        set fpga_part [lshift args]
        puts "INFO: fpga_part = '$fpga_part'"
      }
      -d -
      -design-name {
        set design_name [lshift args]
        puts "INFO: design_name = '$design_name'"
      }
      -x -
      -xdc {
        set xdc_filename [lshift args]
        puts "INFO: xdc_filename = '$xdc_filename'"
      }
      -s -
      -sources {
        set sources_dir [lshift args]
        puts "INFO: sources_dir = '$sources_dir'"
      }
      -t -
      -top-level {
        set top_level_filename [lshift args]
        puts "INFO: top_level = '$top_level_filename'"
      }
      -h -
      -help {
        incr help
      }
      default {
        if {[string match "-*" $flag]} {
          puts " ERROR: option '$flag' is not a valid option."
          incr error
        } else {
          puts "ERROR: option '$flag' is not a valid option."
          incr error
        }
      }
    }
  }

  # print help and exit
  if {$help} {
    set callerflag [lindex [info level [expr [info level] -1]] 0]
    # <-- HELP
    puts [format {
    Usage: %s
    [-part|-p <fpga_part_used>]
    [-design-name|-d <design_name>]
    [-help|-h]

    Description: this tcl_command creates the vivado project for Crono TDC.
    Note: this is prepared to be used with Vivado ${VIVADO_VERSION_REQUIRED}.

    Example:
    %s -p xc7a35ticsg324-1L -d crono_tdc_proof_of_concept

    } $callerflag $callerflag ]
    # HELP -->
    return -code ok {}
  }

  if {${fpga_part} == "none"} {
    puts "\[-p|--part\] required argument was not specified, exiting..."
    return -code error {}
  }

  if {${design_name} == "none"} {
    puts "\[-d|--design-name\] required argument was not specified, exiting..."  
    return -code error {}
  }

  puts "INFO: Creating vivado project!"
  puts "INFO: Design name: ${design_name}"
  puts "INFO: Part used in the design: ${fpga_part}"
  puts "INFO: XDC file used: ${xdc_filename}"
  puts "INFO: Expects to use Vivado ${VIVADO_VERSION_REQUIRED}! if you are not using this version, this might not work as expected!"

  # Set the directory path for the original project.
  # All projects will be in `projects/<design_name>/<design_name>_proj`.
  # The orig_proj_dir will be `<path_to_repo>/projects/<design_name>`.
  set proj_name "${design_name}_proj"
  set orig_proj_dir "[file normalize "${origin_dir}/projects/${design_name}"]"

  # Create project
  create_project ${design_name} ${orig_proj_dir}/${proj_name} -part ${fpga_part} -f

  # Set project properties
  set obj [get_projects ${design_name}]
  set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
  set_property -name "ip_cache_permissions" -value "read write" -objects $obj
  set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
  set_property -name "simulator_language" -value "Mixed" -objects $obj

  # Create 'sources_1' fileset (if not found) and add source files to the 
  # project
  if {[string equal [get_filesets -quiet sources_1] ""]} {
    create_fileset -srcset sources_1
  }

  puts "INFO: Project ${proj_name} in ${orig_proj_dir}/${proj_name}, adding sources..."
  add_files [ glob_for_sources ${sources_dir} ]
  add_files [ glob_for_sources "${orig_proj_dir}/src" ]

  # create needed ip cores
  source "${orig_proj_dir}/create_ip_cores.tcl"

  # Set 'sources_1' fileset properties
  set obj [get_filesets sources_1]

  puts "INFO: Selecting sources top level..."
#  set_property top top_level_crono_tdc [get_filesets sources_1]
  set_property top ${top_level_filename} [get_filesets sources_1]
  set_property top_file {${orig_proj_dir}/src/top_level_${design_name}} [get_filesets sources_1]

  # Create `constrs_1` fileset (if not found) and add constraints of the
  # project
  if {[string equal [get_filesets -quiet constrs_1] ""]} {
    create_fileset -constrset constrs_1
  }

  puts "INFO: adding xdc file ${orig_proj_dir}/xdc/${xdc_filename}..."
  add_files -fileset [get_filesets constrs_1] -norecurse ${orig_proj_dir}/xdc/${xdc_filename}

  # Set 'constrs_1' fileset properties
  set obj [get_filesets constrs_1]

  # Create 'synth_1' run (if not found)
  if {[string equal [get_runs -quiet synth_1] ""]} {
    create_run -name synth_1 -part ${fpga_part} -flow {Vivado Synthesis 2018} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
  } else {
    set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
    set_property flow "Vivado Synthesis 2018" [get_runs synth_1]
  }
  set obj [get_runs synth_1]

  # set the current synth run
  current_run -synthesis [get_runs synth_1]

  # Create 'impl_1' run (if not found)
  if {[string equal [get_runs -quiet impl_1] ""]} {
    create_run -name impl_1 -part ${fpga_part} -flow {Vivado Implementation 2018} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
  } else {
    set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
    set_property flow "Vivado Implementation 2018" [get_runs impl_1]
  }
  set obj [get_runs impl_1]
  set_property -name "steps.write_bitstream.args.readback_file" -value "0" -objects $obj
  set_property -name "steps.write_bitstream.args.verbose" -value "0" -objects $obj

  # set the current impl run
  current_run -implementation [get_runs impl_1]
}

# Check if script is being executed or only sourced
if {[info exists ::argv0] && $::argv0 eq [info script]} {
  puts "script is being executed! creating vivado project..."
  create_vivado_project {*}$argv
}