#!/bin/bash

###############################################################################
## Description:
## Script used to create a Vivado project for Crono TDC Core.
## Author: Julian Nicolas Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>

###############################################################################
## Variables
VIVADO="/opt/Xilinx/Vivado/2018.1/bin/vivado"
FORCE=false
START_GUI=false

POSSIBLE_DESIGN_NAMES=('crono_tdc_lf_arty_a7-35' 'crono_tdc_arty_a7-35' 'crono_tdc_board' 'crono_signal_generator_arty_z7-10')

declare -A FPGA_CHIP

FPGA_CHIP[crono_tdc_lf_arty_a7-35]="xc7a35ticsg324-1L"
FPGA_CHIP[crono_tdc_arty_a7-35]="xc7a35ticsg324-1L"
FPGA_CHIP[crono_tdc_board]="xc7a35tcsg324-2"
FPGA_CHIP[crono_signal_generator_arty_z7-10]="xc7z010clg400-1"

declare -A XDC_FILENAME

XDC_FILENAME[crono_tdc_lf_arty_a7-35]="arty-a7-35.xdc"
XDC_FILENAME[crono_tdc_arty_a7-35]="arty-a7-35.xdc"
XDC_FILENAME[crono_tdc_board]="crono_tdc_board.xdc"
XDC_FILENAME[crono_signal_generator_arty_z7-10]="arty-z7-10.xdc"

declare -A SOURCES_DIR

SOURCES_DIR[crono_tdc_lf_arty_a7-35]="crono_tdc_core/src"
SOURCES_DIR[crono_tdc_arty_a7-35]="crono_tdc_core/src"
SOURCES_DIR[crono_tdc_board]="crono_tdc_core/src"
SOURCES_DIR[crono_signal_generator_arty_z7-10]="crono_signal_generator/src"

declare -A TOP_LEVEL

TOP_LEVEL[crono_tdc_lf_arty_a7-35]="top_level_crono_tdc"
TOP_LEVEL[crono_tdc_arty_a7-35]="top_level_crono_tdc"
TOP_LEVEL[crono_tdc_board]="top_level_crono_tdc"
TOP_LEVEL[crono_signal_generator_arty_z7-10]="top_level_crono_signal_generator"

###############################################################################
## Arguments

help() {
   # display help
   echo "Script used to generate the Vivado Project for the Crono TDC."
   echo
   echo "Syntax:"
   echo "create_vivado_project.sh [-d <DESIGN_NAME>] [-f] [-g] [-h]"
   echo "options:"
   echo "d     Design name. The project will be named <DESIGN_NAME>_proj."
   echo "h     Print this Help."
   echo "f     Force. Erase the project if already exists and create it again."
   echo "g     Start the GUI after creating the project."
   echo
}

while getopts ":d:fgh" opt; do
  case ${opt} in
	d)
        DESIGN_NAME="${OPTARG}"
        PROJ_NAME="${OPTARG}_proj"
      	;;
	f)
        FORCE=true
      	;;
	g)
        START_GUI=true
      	;;
	h)
        help
        exit
      	;;
 	\?)
		echo "Invalid option: -${OPTARG}"
		exit 1
		;;
	:)
        echo "The option -${OPTARG} requires an argument."
        exit 1
      	;;
  esac
done

PROJECT_DIR="projects/${DESIGN_NAME}/${PROJ_NAME}"

###############################################################################
## Functions
log() {
    echo -e "$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"
}


setup() {

    log "Design: ${DESIGN_NAME}\n"
    dn_not_allowed=true
    for possible_dn in "${POSSIBLE_DESIGN_NAMES[@]}"; do
        if [[ ${possible_dn} == ${DESIGN_NAME} ]]; then
            dn_not_allowed=false
            break
        fi
    done
    
    if [[ "${dn_not_allowed}" = "true" ]]; then
        log "${DESIGN_NAME} is not allowed! choose one of the followings: ${POSSIBLE_DESIGN_NAMES[@]}"
        exit 1
    fi

    log "\n Project to be built: ${PROJ_NAME}\n" \
        "FPGA chip: ${FPGA_CHIP[${DESIGN_NAME}]}\n" \
        "XDC file: ${XDC_FILENAME[${DESIGN_NAME}]}\n" \
        "Sources directory: ${SOURCES_DIR[${DESIGN_NAME}]}\n" \
        "Top level: ${TOP_LEVEL[${DESIGN_NAME}]}\n"

    if [ -d "${PROJECT_DIR}" ]; then
        log "${PROJECT_DIR} exists!"
        if [[ ${FORCE} = "true" ]]; then
            log "Force flag set! erasing it and re-creating it..."
            rm -r ${PROJECT_DIR}

            log "Executing vivado in TCL mode to create design ${DESIGN_NAME}..."
            ${VIVADO} -mode batch -source build_project.tcl -tclargs -p ${FPGA_CHIP[${DESIGN_NAME}]} \
                -d ${DESIGN_NAME} \
                -x ${XDC_FILENAME[${DESIGN_NAME}]} \
                -s ${SOURCES_DIR[${DESIGN_NAME}]} \
                -t ${TOP_LEVEL[${DESIGN_NAME}]}
        else
            log "Project already exists.. (if you want to rebuild it, use -f flag)"
        fi
    else
        log "${PROJECT_DIR} do not exists!"
        log "Executing vivado in TCL mode to create design ${DESIGN_NAME}..."
        ${VIVADO} -mode batch -source build_project.tcl -tclargs -p ${FPGA_CHIP[${DESIGN_NAME}]} \
            -d ${DESIGN_NAME} \
            -x ${XDC_FILENAME[${DESIGN_NAME}]} \
            -s ${SOURCES_DIR[${DESIGN_NAME}]} \
            -t ${TOP_LEVEL[${DESIGN_NAME}]}
    fi
}


run() {
    if [[ ${START_GUI} = "true" ]]; then
        log "Starting vivado GUI!..."
        ${VIVADO} "${PROJECT_DIR}/${DESIGN_NAME}.xpr" &
    fi
}


main() {
    setup
    run
}

###############################################################################
main