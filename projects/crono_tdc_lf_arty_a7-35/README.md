# Crono TDC in Arty A7-35

This project is intended to be used with an [Arty A7-35 board](https://digilent.com/shop/arty-a7-artix-7-fpga-development-board/).

This board have a [XC7A35TICSG324-1L](https://docs.xilinx.com/v/u/en-US/ds180_7Series_Overview) FPGA.

# Electrical interface for TDC channels

The FPGA used have 5 High Rank I/O banks, that can support up to 3.3V (see [UG475](https://docs.xilinx.com/v/u/en-US/ug475_7Series_Pkg_Pinout)). These banks are the 14, 15, 16, 34 and 35.

In [Arty A7-35 Schematic](https://digilent.com/reference/_media/arty/arty_sch.pdf) we can see that the board have 4 Pmods, in which 2 of them (JB and JC) are routed as 100Ω differential pairs. So, this Pmods could be used for a high-speed interfaces. 

The pins of JB Pmod are connected to bank 15:
<div align="center">
<img src="img/arty_a7_35_bank15_hs_pmods.png"  width="70%" height="70%">
</div>

And the pins of JC Pmod are connected to bank 14:

<div align="center">
<img src="img/arty_a7_35_bank14_hs_pmods.png"  width="70%" height="70%">
</div>

High Rank banks supports LVDS_25 I/O standard.

In the Arty A7-35 Both banks are powered with 3V3.

**TBC**

# Resources

* [UG471 - SelectIO Resources User Guide](https://docs.xilinx.com/v/u/en-US/ug471_7Series_SelectIO)
* [DS180 - 7 Series FPGAs Data Sheet: Overview](https://docs.xilinx.com/v/u/en-US/ds180_7Series_Overview)
* [UG475 - 7 Series FGPAs Packaging and Pinout Product Specification User Guide](https://docs.xilinx.com/v/u/en-US/ug475_7Series_Pkg_Pinout)
* [Arty A7-35 Schematic](https://digilent.com/reference/_media/arty/arty_sch.pdf)