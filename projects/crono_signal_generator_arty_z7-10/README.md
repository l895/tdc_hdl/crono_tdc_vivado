# Crono TDC in Arty A7-35

This project is intended to be used with an [Arty A7-35 board](https://digilent.com/shop/arty-a7-artix-7-fpga-development-board/).

This board have a [XC7A35TICSG324-1L](https://docs.xilinx.com/v/u/en-US/ds180_7Series_Overview) FPGA.

# Electrical interface for TDC channels

The FPGA used have 5 High Rank I/O banks, that can support up to 3.3V (see [UG475](https://docs.xilinx.com/v/u/en-US/ug475_7Series_Pkg_Pinout)). These banks are the 14, 15, 16, 34 and 35.

In [Arty A7-35 Schematic](https://digilent.com/reference/_media/arty/arty_sch.pdf) we can see that the board have 4 Pmods, in which 2 of them (JB and JC) are routed as 100Ω differential pairs. So, this Pmods could be used for a high-speed interfaces. 

The pins of JB Pmod are connected to bank 15:
<div align="center">
<img src="img/arty_a7_35_bank15_hs_pmods.png"  width="70%" height="70%">
</div>

And the pins of JC Pmod are connected to bank 14:

<div align="center">
<img src="img/arty_a7_35_bank14_hs_pmods.png"  width="70%" height="70%">
</div>

High Rank banks supports LVDS_25 I/O standard, but in Arty A7-35 Both banks are powered with 3V3. So, we can only use them as inputs without internal termination resistors. Is quite hard add this resistors to the dev board because we do not have any solder pad close to fpga pads and by LVDS standard this resistors must be close than 1 cm to receiver. 
So, in this setup we could not test the LVDS interfaces, so, we will use TMDS (Transition Minimized Differential Signaling).
TMDS is a differential I/O standard for transmitting high-speed serial data used by the DVI and HDMI video interfaces. This standard requires external 50 Ohm pull-ups to 3V3 in the inputs. In the FPGA, TMDS is only available in HR I/O banks and requires a VCCO voltage level of
3.3V.


Another thing to see is that we cannot use the pattern generator to loopback the signal to a channel because the ISERDESE2 is a hardware block close to input FPGA input pin, the Xilinx router wannot route a signal from inside from a CLB (Combinational Logic Block) to ISERDESE2 input.

As first tests, we will lower the system frequency to 50MHz and the pattern generator will generate a signal of 200MHz.

**TBC**

# Resources

* [UG471 - SelectIO Resources User Guide](https://docs.xilinx.com/v/u/en-US/ug471_7Series_SelectIO)
* [DS180 - 7 Series FPGAs Data Sheet: Overview](https://docs.xilinx.com/v/u/en-US/ds180_7Series_Overview)
* [UG475 - 7 Series FGPAs Packaging and Pinout Product Specification User Guide](https://docs.xilinx.com/v/u/en-US/ug475_7Series_Pkg_Pinout)
* [Arty A7-35 Schematic](https://digilent.com/reference/_media/arty/arty_sch.pdf)