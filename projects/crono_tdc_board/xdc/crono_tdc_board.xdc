## This file is a .xdc for Crono TDC Board Rev.A.00
## To use it in a project:
## - uncomment the lines corresponding to used pins
## - rename the used ports (in each line, after get_ports) according to the top level signal names in the project

## Clock signal
set_property -dict {PACKAGE_PIN P17 IOSTANDARD LVCMOS33} [get_ports i_base_clk];
create_clock -period 10.000 -name sys_clk_pin -waveform {0.000 5.000} -add [get_ports i_base_clk];

## Clock signal
set_property -dict {PACKAGE_PIN D15 IOSTANDARD LVCMOS33} [get_ports i_aux_clk];
create_clock -period 10.000 -name aux_clk_pin -waveform {0.000 5.000} -add [get_ports i_aux_clk];

## LEDs
set_property -dict { PACKAGE_PIN A14    IOSTANDARD LVCMOS33 } [get_ports { o_led[0] }]; #IO_L9N_T1_DQS_AD3N_15
set_property -dict { PACKAGE_PIN A13    IOSTANDARD LVCMOS33 } [get_ports { o_led[1] }]; #IO_L9P_T1_DQS_AD3P_15

## Button
set_property -dict { PACKAGE_PIN B8    IOSTANDARD LVCMOS33 } [get_ports { i_reset }]; #IO_L12P_T1_MRCC_16

## TDC Channels
# Bank 34
set_property -dict { PACKAGE_PIN L1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[8]}]; # IO_L1P_T0_34
set_property -dict { PACKAGE_PIN M1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[8]}]; # IO_L1N_T0_34

set_property -dict { PACKAGE_PIN K3 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[12]}]; # IO_L2P_T0_34
set_property -dict { PACKAGE_PIN L3 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[12]}]; # IO_L2N_T0_34

set_property -dict { PACKAGE_PIN N2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[7]}]; # IO_L3P_T0_DQS_34
set_property -dict { PACKAGE_PIN N1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[7]}]; # IO_L3N_T0_DQS_34

set_property -dict { PACKAGE_PIN U1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[2]}]; # IO_L7P_T1_34
set_property -dict { PACKAGE_PIN V1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[2]}]; # IO_L7N_T1_34

set_property -dict { PACKAGE_PIN U2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[1]}]; # IO_L9P_T1_DQS_34
set_property -dict { PACKAGE_PIN V2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[1]}]; # IO_L9N_T1_DQS_34

set_property -dict { PACKAGE_PIN V5 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[4]}]; # IO_L10P_T1_34
set_property -dict { PACKAGE_PIN V4 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[4]}]; # IO_L10N_T1_34

set_property -dict { PACKAGE_PIN R3 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[6]}]; # IO_L11P_T1_SRCC_34
set_property -dict { PACKAGE_PIN T3 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[6]}]; # IO_L11N_T1_SRCC_34

set_property -dict { PACKAGE_PIN T5 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[5]}]; # IO_L12P_T1_MRCC_34
set_property -dict { PACKAGE_PIN T4 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[5]}]; # IO_L12N_T1_MRCC_34

set_property -dict { PACKAGE_PIN R1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[3]}]; # IO_L17P_T2_34
set_property -dict { PACKAGE_PIN T1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[3]}]; # IO_L17N_T2_34

set_property -dict { PACKAGE_PIN V7 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[0]}]; # IO_L20P_T3_34
set_property -dict { PACKAGE_PIN V6 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[0]}]; # IO_L20N_T3_34

set_property -dict { PACKAGE_PIN U9 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[17]}]; # IO_L21P_T3_DQS_34
set_property -dict { PACKAGE_PIN V9 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[17]}]; # IO_L21N_T3_DQS_34

set_property -dict { PACKAGE_PIN U7 IOSTANDARD LVDS_25 } [get_ports {o_pattern_generator_p}]; # IO_L22P_T3_34
set_property -dict { PACKAGE_PIN U6 IOSTANDARD LVDS_25 } [get_ports {o_pattern_generator_n}]; # IO_L22N_T3_34

# Bank 35
set_property -dict { PACKAGE_PIN C4 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[16]}]; # IO_L7P_T1_AD6P_35
set_property -dict { PACKAGE_PIN B4 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[16]}]; # IO_L7N_T1_AD6N_35

set_property -dict { PACKAGE_PIN B1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[15]}]; # IO_L9P_T1_DQS_AD7P_35
set_property -dict { PACKAGE_PIN A1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[15]}]; # IO_L9N_T1_DQS_AD7N_35

set_property -dict { PACKAGE_PIN E2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[14]}]; # IO_L14P_T2_SRCC_35
set_property -dict { PACKAGE_PIN D2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[14]}]; # IO_L14N_T2_SRCC_35

set_property -dict { PACKAGE_PIN H2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[13]}]; # IO_L15P_T2_DQS_35
set_property -dict { PACKAGE_PIN G2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[13]}]; # IO_L15N_T2_DQS_35

set_property -dict { PACKAGE_PIN H1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[10]}]; # IO_L17P_T2_35
set_property -dict { PACKAGE_PIN G1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[10]}]; # IO_L17N_T2_35

set_property -dict { PACKAGE_PIN F1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[11]}]; # IO_L18P_T2_35
set_property -dict { PACKAGE_PIN E1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[11]}]; # IO_L18N_T2_35

set_property -dict { PACKAGE_PIN K2 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_p[9]}]; # IO_L23P_T3_35
set_property -dict { PACKAGE_PIN K1 IOSTANDARD LVDS_25 } [get_ports {i_channels_inputs_n[9]}];; # IO_L23N_T3_35


## AUXIO
#set_property -dict { PACKAGE_PIN A18   IOSTANDARD LVCMOS33 } [get_ports {auxio_15[0]}];
#set_property -dict { PACKAGE_PIN A16   IOSTANDARD LVCMOS33 } [get_ports {auxio_15[1]}];
#set_property -dict { PACKAGE_PIN A11   IOSTANDARD LVCMOS33 } [get_ports {auxio_15[2]}];
#set_property -dict { PACKAGE_PIN A10   IOSTANDARD LVCMOS33 } [get_ports {auxio_16}];
#set_property -dict { PACKAGE_PIN A9    IOSTANDARD LVCMOS33 } [get_ports {i2c_fmc_scl}];
#set_property -dict { PACKAGE_PIN A8    IOSTANDARD LVCMOS33 } [get_ports {i2c_fmc_sda}];


## USB-UART Interface
set_property -dict {PACKAGE_PIN B18     IOSTANDARD LVCMOS33} [get_ports o_tx_serial];
set_property -dict {PACKAGE_PIN E18     IOSTANDARD LVCMOS33} [get_ports i_rx_serial];
#set_property -dict {PACKAGE_PIN F18    IOSTANDARD LVCMOS33} [get_ports cts];
#set_property -dict {PACKAGE_PIN D18    IOSTANDARD LVCMOS33} [get_ports rts];


## Cubesat Kit Bus
# Bank 15
#set_property -dict { PACKAGE_PIN G18   IOSTANDARD LVCMOS33 } [get_ports {pc104_io15[0]}];
#set_property -dict { PACKAGE_PIN J18   IOSTANDARD LVCMOS33 } [get_ports {pc104_io15[1]}];

# Bank 14
#set_property -dict { PACKAGE_PIN L18   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[0]}];
#set_property -dict { PACKAGE_PIN T18   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[1]}];
#set_property -dict { PACKAGE_PIN U18   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[2]}];
#set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[3]}];
#set_property -dict { PACKAGE_PIN V16   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[4]}];
#set_property -dict { PACKAGE_PIN V17   IOSTANDARD LVCMOS33 } [get_ports {pc104_io14[5]}];
