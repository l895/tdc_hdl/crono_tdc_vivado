# Create Clocking wizard ######################################################
# TCL commands taken from vivado when integrating a clocking wizard
# with four clock outputs with phaseshifts of 90ª between them
# https://support.xilinx.com/s/question/0D52E00006iHvqgSAC/clocking-wizard-configuration-problem?language=en_US
# example to get tcl code from ip in vivado: write_ip_tcl [get_ips clk_core] tt.tcl
#set clk_wiz_0 [create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_wiz_0]

# set_property -dict { 
#     CONFIG.CLKOUT2_USED {true}
#     CONFIG.CLKOUT3_USED {true}
#     CONFIG.CLKOUT4_USED {true}
#     CONFIG.NUM_OUT_CLKS {4}
#     CONFIG.PRIMARY_PORT {i_clk}
#     CONFIG.CLK_OUT1_PORT {o_clk}
#     CONFIG.CLK_OUT2_PORT {o_oclk}
#     CONFIG.CLK_OUT3_PORT {o_clkb}
#     CONFIG.CLK_OUT4_PORT {o_oclkb}
#     CONFIG.CLKOUT2_REQUESTED_PHASE {90}
#     CONFIG.CLKOUT3_REQUESTED_PHASE {180}
#     CONFIG.CLKOUT4_REQUESTED_PHASE {270}
#     CONFIG.MMCM_CLKOUT1_DIVIDE {10}
#     CONFIG.MMCM_CLKOUT1_PHASE {90.000}
#     CONFIG.MMCM_CLKOUT2_DIVIDE {10}
#     CONFIG.MMCM_CLKOUT2_PHASE {180.000}
#     CONFIG.MMCM_CLKOUT3_DIVIDE {10}
#     CONFIG.MMCM_CLKOUT3_PHASE {270.000}
#     CONFIG.CLKOUT2_JITTER {130.958}
#     CONFIG.CLKOUT2_PHASE_ERROR {98.575}
#     CONFIG.CLKOUT3_JITTER {130.958}
#     CONFIG.CLKOUT3_PHASE_ERROR {98.575}
#     CONFIG.CLKOUT4_JITTER {130.958}
#     CONFIG.CLKOUT4_PHASE_ERROR {98.575}
# } [get_ips clk_wiz_0]

# set_property -dict { 
#     GENERATE_SYNTH_CHECKPOINT {1}
# } $clk_wiz_0

set clk_wiz clk_wiz_0
create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name $clk_wiz

set_property -dict { 
  CONFIG.CLKOUT2_USED {true}
  CONFIG.CLKOUT3_USED {true}
  CONFIG.CLKOUT4_USED {false}
  CONFIG.NUM_OUT_CLKS {3}
  CONFIG.PRIMARY_PORT {i_clk}
  CONFIG.CLK_OUT1_PORT {o_clk}
  CONFIG.CLK_OUT2_PORT {o_serdes_clk}
  CONFIG.CLK_OUT3_PORT {o_serdes_oclk}
  CONFIG.CLK_OUT4_PORT {o_pattern_clk}
  CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {100}
  CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {100}
  CONFIG.CLKOUT2_REQUESTED_PHASE {0}
  CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {100}
  CONFIG.CLKOUT3_REQUESTED_PHASE {90}
  CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {100}
  CONFIG.MMCM_DIVCLK_DIVIDE {1}
  CONFIG.MMCM_CLKFBOUT_MULT_F {10.000}
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {10.000}
  CONFIG.MMCM_CLKOUT1_DIVIDE {10}
  CONFIG.MMCM_CLKOUT1_PHASE {0.000}
  CONFIG.MMCM_CLKOUT2_DIVIDE {10}
  CONFIG.MMCM_CLKOUT2_PHASE {90.000}
  CONFIG.MMCM_CLKOUT3_DIVIDE {1}
  CONFIG.CLKOUT1_JITTER {130.958}
  CONFIG.CLKOUT1_PHASE_ERROR {98.575}
  CONFIG.CLKOUT2_JITTER {130.958}
  CONFIG.CLKOUT2_PHASE_ERROR {98.575}
  CONFIG.CLKOUT3_JITTER {130.958}
  CONFIG.CLKOUT3_PHASE_ERROR {98.575}
  CONFIG.CLKOUT4_JITTER {130.958}
  CONFIG.CLKOUT4_PHASE_ERROR {98.575}
} [get_ips $clk_wiz]

generate_target {instantiation_template} [get_ips $clk_wiz]

set clk_wiz1 clk_wiz_1
create_ip -name clk_wiz1 -vendor xilinx.com -library ip -version 6.0 -module_name $clk_wiz1

set_property -dict { 
  CONFIG.PRIMARY_PORT {i_clk}
  CONFIG.CLK_OUT1_PORT {o_clk}
  CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {400}
  CONFIG.MMCM_DIVCLK_DIVIDE {1}
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {2.500}
  CONFIG.CLKOUT1_JITTER {101.114}
} [get_ips $clk_wiz1]

generate_target {instantiation_template} [get_ips $clk_wiz1]
