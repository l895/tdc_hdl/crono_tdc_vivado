# crono_tdc_vivado

Vivado project for Crono TDC.

This project generates the Vivado project for the Crono TDC using a simple Tcl script.
The idea was based in this other repository: https://github.com/fpgadeveloper/microzed-base/blob/master/Vivado/build.tcl

## Directory structure

The directory structure of the repository is the following:

```shell
├── build_project.tcl               ## super cool TCL script to build projects
├── create_vivado_project.sh        ## the bash script that must be used to build the projects
├── crono_tdc_core                  ## submodule with all the hdl sources of crono TDC
├── projects                        ## directory with the designs in question
│   └── crono_tdc_lf                ## crono_tdc_lf design 
│       ├── create_ip_cores.tcl     ## super cool TCL script that build the xilinx IP cores of the design
│       ├── crono_tdc_lf_proj       ## directory where will be the Vivado project of the design
│       ├── src                     ## particular sources for the design
│       └── xdc                     ## constraints files for the design 
├── README.md

```

## Where are the hdl sources?

The `crono_tdc_sources` are included as a git submodule. So, first, check that he submodule is initiated. If not, run this command `git submodule update --recursive --remote` to get the submodule files (if you clone the repository `normally` you will not get this files).

## Building the project

The project can be built using `create_vivado_project.sh` script. We can choose the design that we wants to build and then in the folder `projects/<design_name>/<design_name>_proj` will be the Vivado project of the design. With this script we can launch the Vivado GUI too opened in the project.

## Troubleshooting

### Hardware manager do not detect Arty board
In this project we have some projects that are built for Arty A7-35 Board. If this is the case, may be you will need to install the digilent cable drivers, you can install them from here: `https://digilent.com/reference/software/adept/start?redirect=2#software_downloads`.

Install `runtime` and `utilities`. Then, you can check if the board is alive plugging it to the PC and running this command `djtgcfg enum`. This will show the device if it is found:

```shell
Found 1 device(s)

Device: Arty
    Device Transport Type: 00020001 (USB)
    Product Name:          Digilent Arty
    User Name:             Arty
    Serial Number:         210319A2CD81
```